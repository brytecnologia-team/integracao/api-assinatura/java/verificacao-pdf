package br.com.bry.framework.verificacao.main;

import br.com.bry.framework.verificacao.VerifierPDF;

public class Application {

	public static void main(String[] args) {
		VerifierPDF pdfVerification = new VerifierPDF();

		System.out.println(
				"=====================================Verification of PDF Signature:=====================================");

		pdfVerification.verifyPDFSignature();
	}

}
