package br.com.bry.framework.verificacao.config;

public class ServiceConfig {

	public static final String URL_PDF_VERIFIER = "https://fw2.bry.com.br/api/pdf-verification-service/v1/signatures/verify";

	public static final String ACCESS_TOKEN = "<INSERT_VALID_ACCESS_TOKEN>";
}
